const cacheFunction = require('../cacheFunction.cjs');

let factorial = (number) => {
    let factorial = 1;
    while(number > 0){
        factorial *= number--;
    }
    return factorial;
}

let cachedFactorial = cacheFunction(factorial);

console.log('using cachedfactorial function :');
console.log('first time call');
console.time();
console.log(cachedFactorial(50));
console.timeEnd()

console.log('--------------------------\n');

console.log('second time call');
console.time();
console.log(cachedFactorial(50));
console.timeEnd()
