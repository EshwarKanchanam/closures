function cacheFunction(callback) {

    const cache = {};
    return function (argument) {
        if (cache[argument] === undefined) {
            cache[argument] = callback(argument);
        }
        return cache[argument];
    }
}

module.exports = cacheFunction;