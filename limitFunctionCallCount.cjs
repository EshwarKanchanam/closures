function limitFunctionCallCount(callback, numberOfTimesCallbackInvokes) {

    return function () {
        while (numberOfTimesCallbackInvokes-- > 0) {
            return callback();
        }
        return null;
    }
}

module.exports = limitFunctionCallCount;